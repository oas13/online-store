<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Location;

class UserController extends Controller
{
    //
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $data = [];
        $errors = [];
        $regSubmit = $this->request->input('regSubmit');
        $loginSubmit = $this->request->input('loginSubmit');

        if (isset($regSubmit))
        {
            $registration = $this->registration();
            if (isset($registration['data']))
                $data = $registration['data'];
            if (isset($registration['errors']))
                $errors = $registration['errors'];
        }

        if (isset($loginSubmit))
        {
            if($this->login())
                return redirect('/');
        }


        return view('user.login', compact('data', 'errors'));
    }

    public function login()
    {
        $email = $this->request->get('loginEmail');
        $user = User::where('email', $email)->first();
        $password = $this->request->get('loginPassword');
        if ($user && Hash::check($password, $user->password))
        {
            session(['userId' => $user->id]);

            return redirect('/');
        }

    }

    public function registration()
    {
        $name = $this->request->input('name');
        $surname = $this->request->input('surname');
        $email = $this->request->input('email');
        $pass = $this->request->input('password');
        $passConfirm = $this->request->input('passConfirm');
        $result = $this->checkRegistrationData($name, $surname, $email, $pass, $passConfirm);

        if (!isset($result['errors']))
        {
            $password = Hash::make($pass);

            User::create([
                'name' => $name,
                'surname' => $surname,
                'email' => $email,
                'password' => $password
            ]);
        }

        return $result;
    }

    public function logout()
    {
        session()->flush();

        return redirect('/');
    }

    public function profile()
    {
        $success = '';
        $user = User::find(session('userId'));

        if($this->request->get('changeData'))
        {
            $this->profileEdit($user);
            if($this->profileEdit($user))
                $success = 'Успешно обновлено';
        }


        return view('user.profile', compact('success', 'user'));
    }

    public function profileEdit($user)
    {
        $name = $this->request->input('name');
        $surname = $this->request->input('surname');
        $phone = $this->request->input('phone');
        $oldPass = $this->request->input('oldPassword');
        $newPass = $this->request->input('newPassword');
        $passConfirm = $this->request->input('passwordConfirm');

        $result = $this->checkProfileData($name, $surname, $phone, $oldPass, $newPass, $passConfirm);
        if(!isset($result['error']) && isset($user))
        {
            $user->update([
               'name' => $name,
               'surname' => $name
            ]);

            return true;
        }
    }


    protected function checkRegistrationData($name, $surname, $email, $pass, $passConfirm)
    {
        if (isset($name))
            $regData['data']['name'] = $name;
        else
            $regData['errors']['name'] = 'Ввведите имя';

        if (isset($surname))
            $regData['data']['surname'] = $surname;
        else
            $regData['errors']['surname'] = 'Ввведите фамилию';

        if (isset($email)) {
            // TODO:: добавить валидацию для email
            if(!User::where('email', $email)->first())
                $regData['data']['email'] = $email;
            else
                $regData['errors']['email'] = 'Такая почта уже используется';
        } else
            $regData['errors']['email'] = 'Введите email';

        if (isset($pass)) {
            if (strlen($pass) < 3)
                $regData['errors']['pass'] = 'Пароль должен быть больше 3 сммволов';
            else
                $regData['data']['name'] = $name;
        } else
            $regData['errors']['pass'] = 'Введите пароль';

        if (isset($passConfirm)) {
            if ($passConfirm == $pass)
                $regData['data']['name'] = $name;
            else
                $regData['errors']['passConfirm'] = 'Пароли должны совпадать';
        } else
            $regData['errors']['passConfirm'] = 'Введите пароль для подтверждения';

        return $regData;
    }

    protected function checkProfileData($name, $surname, $phone, $oldPassword,
                                        $newPassword, $passConfirm)
    {

    }
}
