<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;

class HomeController extends Controller
{
    //
    public function index()
    {
        $featuredProducts = $this->getFeaturedProducts();
        $user = null;
        $userId = session()->get('userId');
        if($userId)
            $user = User::find($userId);

        return view('home', compact('featuredProducts', 'user'));
    }

    public function getFeaturedProducts()
    {
        $products = Product::where('featured', 1)->get();

        return $products;
    }
}
