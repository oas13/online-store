<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01/03/2018
 * Time: 22:04
 */

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $items = null;
        if (session('cart'))
            $items = $this->getItems();

        return view('cart.index', compact('items'));
    }

    public function addItem($id)
    {
        $product = Product::find($id);
        $items = $this->getItems();
        if (!$items)
        {
            $items[$id] = [
                'count' => 1,
                'price' => $product->price,
                'product' => $product
            ];
        }
        else
        {
            if (array_key_exists($id, $items))
            {
                $items[$id]['count']++;
            }
            else
            {
                $items[$id] = [
                    'count' => 1,
                    'price' => $product->price,
                    'product' => $product
                ];
            }
        }
        $totalPrice = $this->getTotal($items)['totalPrice'];
        $totalAmount = $this->getTotal($items)['totalAmount'];
        session()->put('cart' , [
            'items' => $items,
            'totalAmount' => $totalAmount,
            'totalPrice' => $totalPrice
        ]);

        return redirect()->back();
    }

    public function deleteItem($id)
    {
        session()->forget("cart.items.$id");
        $items = $this->getItems();
        $totalPrice = $this->getTotal($items)['totalPrice'];
        $totalAmount = $this->getTotal($items)['totalAmount'];
        session()->put('cart.totalAmount', $totalAmount);
        session()->put('cart.totalPrice', $totalPrice);

        return redirect()->back();
    }

    public function getTotal($items)
    {
        $total = [];
        $totalPrice = 0;
        $totalAmount = 0;

        foreach ($items as $item) {
            $totalPrice += $item['price'] * $item['count'];
            $totalAmount += $item['count'];
        }
        $total['totalPrice'] = $totalPrice;
        $total['totalAmount'] = $totalAmount;

        return $total;
    }

    private function getItems()
    {
        $cart = session('cart');

        return $cart['items'];
    }
}