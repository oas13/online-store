<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 21/02/2018
 * Time: 21:59
 */

namespace App\Http\Controllers;


use App\Models\Categories;
use App\Models\Product;

class CatalogController extends Controller
{
    public function index()
    {
        $products = Product::where('featured', 0)->paginate(5);
        $categories = $this->getCategories();

        return view('catalog', compact('products', 'categories'));
    }

    public function getCategories()
    {
        $categories = Categories::all();

        return $categories;
    }

    public function productsByCategory($categoryId)
    {
        $categories = $this->getCategories();
        $products = Product::where('category_id', $categoryId)->paginate(6);

        return view('catalog', compact('products', 'categories'));
    }

}