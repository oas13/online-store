<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26/02/2018
 * Time: 20:55
 */
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class User extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'user';
    }
}