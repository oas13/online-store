<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //
    protected $table = 'categories';

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_category',
            'category_id', 'product_id');
    }
}
