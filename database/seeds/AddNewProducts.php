<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class addNewProducts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        dump(Product::first());
        Product::create([
            'name' => 'Product1',
            'art' => 'Prod1',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 100,
            'old_price' => 140,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
            'name' => 'Product2',
            'art' => 'Prod2',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 90,
            'old_price' => 120,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
            'name' => 'Product3',
            'art' => 'Prod3',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 12,
            'old_price' => 40,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
            'name' => 'Product4',
            'art' => 'Prod4',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 10,
            'old_price' => 14,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
            'name' => 'Product5',
            'art' => 'Prod5',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 134,
            'old_price' => 1350,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
            'name' => 'Product6',
            'art' => 'Prod6',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 1000,
            'old_price' => 14000,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
            'name' => 'Product7',
            'art' => 'Prod7',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 23,
            'old_price' => 40,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
            'name' => 'Product8',
            'art' => 'Prod8',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 34,
            'old_price' => 56,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
        'name' => 'Product9',
        'art' => 'Prod9',
        'img' => 'img/1.jpg',
        'category_id' => 1,
        'price' => 98,
        'old_price' => 76,
        'preview_description' => 'Lorem ipsum dolor sit amet',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
        'featured' => 1,
        'sort' => 1
    ]);
        Product::create([
            'name' => 'Product10',
            'art' => 'Prod10',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 53,
            'old_price' => 98,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
            'name' => 'Product11',
            'art' => 'Prod11',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 25,
            'old_price' => 50,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
        Product::create([
        'name' => 'Product12',
        'art' => 'Prod12',
        'img' => 'img/1.jpg',
        'category_id' => 1,
        'price' => 11,
        'old_price' => 21,
        'preview_description' => 'Lorem ipsum dolor sit amet',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
        'featured' => 1,
        'sort' => 1
    ]);Product::create([
        'name' => 'Product13',
        'art' => 'Prod13',
        'img' => 'img/1.jpg',
        'category_id' => 1,
        'price' => 30,
        'old_price' => 40,
        'preview_description' => 'Lorem ipsum dolor sit amet',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
        'featured' => 1,
        'sort' => 1
    ]);
        Product::create([
            'name' => 'Product14',
            'art' => 'Prod14',
            'img' => 'img/1.jpg',
            'category_id' => 1,
            'price' => 51,
            'old_price' => 75,
            'preview_description' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, sit.',
            'featured' => 1,
            'sort' => 1
        ]);
    }
}
