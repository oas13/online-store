<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'HomeController@index');
Route::get('catalog', 'CatalogController@index');
Route::get('product/{id}', 'ProductController@index');
Route::get('catalog/category/{id}', 'CatalogController@productsByCategory');
Route::group(['prefix' => 'user'], function()
{
    Route::post('login-page/registration', 'UserController@registration');
    Route::any('login-page', 'UserController@index');
    Route::any('logout', 'UserController@logout');
    Route::any('profile', 'UserController@profile');
    Route::any('profile/edit', 'UserController@profile');
});

Route::get('cart', 'CartController@index');
Route::get('cart/add/product/{id}', 'CartController@addItem');
Route::post('cart/delete/product/{id}', 'CartController@deleteItem')->name('deleteItem');
