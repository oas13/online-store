<nav class="navbar navbar-default" role="navigation">
    <div class="content">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" id="logo-mob" href="/">
                <img src="{{ asset('img/logo.png') }}" alt=""/>
            </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse">

            <ul class="nav navbar-nav">
                <li>
                    <a class="navbar-brand" id="logo" href="/">
                        <img src="{{ asset('img/logo.png') }}" alt=""/>
                    </a>
                </li>
                <li>
                    <a href="/catalog">Catalog</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li>
                    @if(session('userId'))
                    <p>Welcome, <a href="/user/profile">{{ \App\Models\User::find(session('userId'))->name }}</a>!
                        <a href="/user/logout">Logout</a>
                    </p>
                    @else
                        <a href="/user/login-page">Login</a>
                    @endif
                </li>
                <li id="normal-cart">
                    <a id="normal-carts" class="dropdown-toggle" href="#" data-toggle="dropdown" role="button">
                        Item
                        <span class="badge">
                            {{ session('cart')['items'] ? session('cart')['totalAmount'] : '0' }}
                        </span>
                    </a>
                    <div id="mini-cart" class="dropdown-menu">
                        @if(session('cart')['items'])
                        <ul class="cart-items list-unstyled hidden-xs">
                            @foreach(session('cart')['items'] as $item)
                            <li class="row cart-item">
                                <div class="product-thumb col-xs-4">
                                    <img class="img-responsive" alt="{{ $item['product']->name }}" src=" /{{ $item['product']->img }}" style="width: 100px; height: auto">
                                </div>
                                <div class="product-info col-xs-8">
                                    <a class="remove-item pull-right" href="/cart/delete/product/{{ $item['product']->id }}"><i class="fa fa-times"></i></a>
                                    <h6 class="product-title">{{ $item['product']->name }}

                                    </h6>

                                    <div class="price">
                                        {{ $item['count'] }} x <span>{{ $item['price'] }}</span>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <div class="mini-cart-totals">
                            <h4 class="subtotal">Subtotal: ${{ session('cart')['totalPrice'] }}</h4>
                            <a class="btn btn-default col-xs-12" href="/cart">View Cart</a>
                            {{--{% if customer %}--}}
                            <a class="col-xs-12 btn btn-default btn-important" href="/checkout">Checkout</a>
                            {{--{% else %}--}}
                            {{--<a class="col-xs-12 btn btn-important" href="{{ site_url('/checkout-start') }}">Checkout</a>--}}
                            {{--{% endif %}--}}
                        </div>
                        @else
                        <h6>No items in cart.</h6>
                        {{--{% endif %}--}}
                        @endif
                    </div>
                </li>
                <li id="mobile-cart">
                    <a href="/cart">View Cart</a>
                </li>
                <li id="mobile-cart">
                    {{--{% if customer %}--}}
                    {{--<a href="{{ site_url('checkout') }}" >Checkout</a>--}}
                    {{--{% else %}--}}
                    {{--<a href="{{ site_url('checkout-start') }}" >Checkout</a>--}}
                    {{--{% endif %}--}}
                </li>

            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
{{--<div class="row content breadcrumb-container">--}}
    {{--<div class="col-sm-12">--}}

    {{--</div>--}}
{{--</div>--}}
{{--@section('javascripts')--}}
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$('#normal-carts').mouseover(function () {--}}
                {{--$('.dropdown-menu').show()--}}
            {{--})--}}
            {{--$('#normal-carts').mouseout(function () {--}}
                {{--$('.dropdown-menu').hide();--}}
            {{--})--}}
        {{--})--}}
    {{--</script>--}}
{{--@endsection--}}
