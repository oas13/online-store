<!DOCTYPE html>
<html>
<head>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <title>Title</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="Description" content=""/>
    <meta name="Keywords" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="" type="image/png">
    <link rel="icon" href="{{ asset('img/icon.png') }}" type="image/png">

    {{--<link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>--}}
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/combined.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/checkout.css') }}">
    @yield('css')
    {{--<link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">--}}

    {{--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--}}
    {{--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>--}}

    <!-- THEME CUSTOMiZATION OPTIONS -->
    <style type="text/css">
        .navbar-default .navbar-nav > li > a{
            color: black;
        }
        .navbar{
            /*background: ;*/
        }
        .main-footer{
            background: #F5F;
        }
        #intro-left{
            background: url({{ asset('img/bg1.jpg') }})no-repeat top center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        #intro-right{
            background: url({{ asset('img/bg2.jpg') }})no-repeat top center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .btn-important{
            {{--background: {{ theme.importantButton }};--}}
        }
        .payment-button{
            {{--background: {{ theme.payButton }};--}}
        }
    </style>

</head>
<body>
    {{-------HEADER--------}}
        @include('layout.header')

    {{------CONTENT------}}
    <div class="row full-width">
        @yield('content')
    </div>
    {{--FOOTER--}}

        @include('layout.footer')


    <script type="text/javascript" src="{{ asset('javascript/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.remove-item').click(function () {
                let uri=$(this).attr('href');
                $.ajax({
                    url: uri,
                    data: 'add',
                    type: 'POST',
                    success: function () {
                        window.location.reload()
                    }
                });
            })
        })
    </script>
    @yield('javascripts')
</body>
</html>
