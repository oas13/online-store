<div class="jumbotron" id="main-footer">
    <div class="row content">
        <div class="col-md-8">
            <ul class="list-unstyled col-md-4">
                <li class="title">SHOP</li>
                <a href="/shop"><li>All</li></a>
                {{--{% for category in tree.items %}--}}
                {{--<a href="/category/{{ category.fullUrlName }}"><li>{{ category.name }}</li></a>--}}
                {{--{% endfor %}--}}
            </ul>
        </div>
        <div class="col-xs-6 col-md-4" id="foot-info">
            <p>This is Slate Free Theme <br>
                Powered by <a href="https://lemonstand.com/" target="_blank">LemonStand</a><br>
            </p>
        </div>
    </div>
</div>