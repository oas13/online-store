@extends('layout.main')

@section('content')
    <div class="row content" id="cart">
        <p id="title-p-d">Your Cart</p>
        <div class="col-md-12">
            @if($items)
                <div class="items-holder">
                    <div class="row content" id="cart-items">
                        <div class="col-sm-6">Товар</div>
                        <div class="col-sm-3">Количество</div>
                        <div class="col-sm-3">Item Total</div>
                    </div>
                    @foreach($items['items'] as $item)
                        <div class="row" id="cart-row">
                            <div class="col-sm-2" id="cart-img">
                                <img src="{{ $item['product']->img }}" alt="prod_name">
                            </div>
                            <div class="row" id="align-middle">
                                <div class="col-sm-4">
                                    <a href="/product/{{ $item['product']->id }}">
                                        {{ $item['product']->name }}
                                    </a>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="quantity" value="{{ $item['count'] }}">
                                </div>
                                <div class="col-sm-3 col-xs-12" id="item-middle">
                                    <span class="pull-left">${{ $item['price'] }}</span>
                                    <a class="col-sm-3" id="item-close" href="{{ route('deleteItem', [$item['product']->id]) }}">
                                        <i class="fa fa-times" id="remove-cart-item"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div id="cart-actions" class="btn-holder clearfix">
                    <a class="" href="/catalog">Continue Shopping</a>
                </div>
        </div>

        <div class="col-md-4" id="summary">
            <p id="title-p-d">Order Summary</p>
            <ul class="price-list list-group">
                {{--{% if totals.discountTotal > 0 %}--}}
                <li class="list-group-item">Discount:</li>
                {{--{% endif %}--}}
                {{--{% if totals.totalShippingQuote > 0 %}--}}
                <li class="list-group-item">Shipping:</li>
                {{--{% endif %}--}}
                <li class="list-group-item important">Total: ${{ $items['totalPrice'] }}</li>
            </ul>
            <div class="col-xs-12 form-group" id="coupon">
                <input type="text" class="form-control" id="coupon-code" name="coupon" placeholder="Coupon Code"
                       value=""/>
            </div>
            <a class="btn btn-default col-xs-12 btn-lg solid"
               href="#" data-ajax-handler="shop:cart"
               data-ajax-update="#cart-content=shop-cart-content, #mini-cart=shop-minicart, #navbar-totals=shop-minicart-totals">
                Update Cart <i class="fa fa-refresh"></i>
            </a>
            {{--{% if customer %}--}}
            <a class="col-xs-12 btn btn-important solid btn-lg" href="/checkout">Checkout</a>
            {{--{% else %}--}}
            {{--<a class="col-xs-12 btn btn-important solid btn-lg" href="/checkout-start">Checkout</a>--}}
            {{--{% endif %}--}}
        </div>
    </div>
    @else
        <p>There are no Items in your cart!</p>
    @endif
@endsection