@extends('layout.main')

@section('content')
    <div class="row content">
        <div class="col-md-6">
            <a href="">
                <img src="{{ asset($product->img) }}" alt=""
                     class="img-responsive normal">
            </a>
        </div>
        <div id="product-detail" class="col-md-6">
            <div class="col-md-8">
                @if($product->featured)
                    <small class="previous-price">
                        <i class="fa fa-dollar"></i>
                        {{ $product->old_price }}
                    </small>
                @endif
                <h3><i class="fa fa-dollar"></i>{{ $product->price }}</h3>
                <p>This desk is perfect for the bedside. Configure it in ways that fit your needs.&nbsp;</p><p></p>
            </div>

            <div class="col-md-4" id="attributes">
                <div class="product-options">
                    <label class="title" for="option-0">Color</label><br>
                    <select id="option-0" name="options[4]" class="select-option" data-ajax-handler="shop:product" data-ajax-update="#product-page=shop-product">
                        <option value="11">Black</option>
                        <option value="12">White</option>
                        <option value="13">Gray</option>
                    </select>
                </div>
                <input type="hidden" name="productId" value="9">
                <div class="add-cart-holder form-group">

                    <div class="quantity-selector">
                        <label class="title">Quantity</label>
                        <input class="form-control quantity" type="text" value="1" name="quantity">
                    </div>
                </div>

                <a class="btn btn-important btn-add-cart" data-ajax-handler="shop:onAddToCart" data-ajax-update="#normal-carts=shop-minicart-totals, #product-page=shop-product, #mini-cart=shop-minicart">Add to Cart </a>
            </div>
        </div>
    </div>
@endsection