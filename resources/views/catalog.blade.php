@extends('layout.main')

@section('content')
    <div class="row content">
        <div class="col-md-3" id="categories">
            <ul class="categories_list">
                @foreach($categories as $category)
                    <li>
                        <a href="/catalog/category/{{ $category->id }}">
                            {{ mb_strtoupper($category->name) }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="filters col-md-9">
            <select name="price" id="">
                <option value="1">По убыванию</option>
                <option value="2">По возрастанию</option>
            </select>
            <select name="material" id="">
                <option value="3">Wood</option>
                <option value="4">Metal</option>
            </select>
        </div>
        <div class="col-md-9">
            <div class="row">
                @foreach($products as $product)
                        <div class="col-md-4 shop-item">
                            <a href="/product/{{ $product->id }}">
                                <div class="add_cart col-md-12">
                                    <a href="/cart/add/product/{{ $product->id }}" class="btn btn-success">
                                        Add to cart
                                    </a>
                                    <a href="/product/{{ $product->id }}" class="btn btn-info">Preview</a>
                                </div>
                            <img src="{{ asset($product->img) }}" alt="" class="prod_img">
                            <div class="item-detail">
                                <h2>{{ $product->name }}</h2>
                                <small class="previous-price">
                                    <i class="fa fa-dollar"></i>
                                    {{ $product->old_price }}
                                </small>
                                <p>
                                    <i class="fa fa-dollar"></i>
                                    {{ $product->price }}
                                </p>
                            </div>
                            </a>
                        </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="pagination-centered">
        {{ $products->links() }}
    </div>
@endsection

@section('javascripts')
    <script>
        $(document).ready(function () {
            var categoryId = location.href.split('/').pop();
            $('.categories_list li').each(function () {
                var categoryElement = $(this).find('a').attr('href').split('/').pop();
                if (categoryElement == categoryId)
                    $(this).addClass('active');
            });
//            $('.shop-item').hover(function () {
//                $('.add_cart', this).animate({height:"toggle"}, 'liner')
//            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            })
            $('.add_item').click(function (e) {
                e.preventDefault();
                let uri = $(this).attr("href")
                $.ajax({
                    url: uri,
                    data: 'add',
                    type: 'POST',
                    success: function (data) {
                        console.log(data)
                    }
                });
            })
        })
    </script>
@endsection