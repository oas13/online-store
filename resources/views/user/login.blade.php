@extends('layout.main')

@section('content')
    <div class="content" id="login-page">
        <div class="row">
            <div class="col-sm-6">
                <p id="title-p">RETURNING CUSTOMERS</p>
                <form class="form-horizontal" method="post" action="/user/login-page">
                    @csrf
                    <diцv class="form-group">
                        <label for="login_email" class="control-label">Email</label><br>
                        <div class="col-sm-9">
                            <input type="email" class="form-control input-sm" id="login_email" name="loginEmail" value=""
                                   size="40" autofocus="">
                            <span class="error small text-danger"></span>
                        </div>
                    </diцv>
                    <div class="form-group">
                        <label for="login_password" class="control-label">Password</label><br>
                        <div class="col-sm-9">
                            <input type="password" class="form-control input-sm" id="login_password" name="loginPassword"
                                   size="20">
                            <span class="error small text-danger"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="submit" class="btn btn-important" name="loginSubmit" value="Login">
                            <a class="btn btn-sm btn-link forgotpass" href="login/password-restore">Forgot
                                your password?</a>
                        </div>
                    </div>

                </form>
            </div>

            <div class="col-sm-6">
                <p id="title-p">NEW CUSTOMERS</p>

                <form class="form-horizontal" method="post" action="/user/login-page">
                    @csrf
                    <div class="form-group">
                        <label for="signup[first_name]" class="control-label">First Name</label><br>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm"
                                   name="name" value="{{ isset($data['name']) ? $data['name'] : '' }}" size="16">
                            <span class="error small text-danger">{{ isset($errors['name']) ? $errors['name'] : '' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="signup[last_name]" class="control-label">Last Name</label><br>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm"
                                   name="surname" value="{{ isset($data['surname']) ? $data['surname'] : '' }}" size="16">
                            <span class="error small text-danger">{{isset($errors['surname']) ? $errors['surname'] : '' }}</span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="signup[email]" class="control-label">Email</label><br>
                        <div class="col-sm-9">
                            <input type="email" class="form-control input-sm" name="email"
                                   value="{{ isset($data['email']) ? $data['email'] : '' }}" size="40">
                            <span class="error small text-danger">{{isset($errors['email']) ? $errors['email'] : '' }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signup[password]" class="control-label">Password</label><br>
                        <div class="col-sm-9">
                            <input type="password" class="form-control input-sm"
                                   name="password" value="" size="16">
                            <span class="error small text-danger">{{ isset($errors['pass']) ? $errors['pass'] : '' }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signup[password_confirmation]" class="control-label">Confirm</label><br>
                        <div class="col-sm-9">
                            <input type="password" class="form-control input-sm"
                                   name="passConfirm" value="" size="16">
                            <span class="error small text-danger">{{ isset($errors['passConfirm']) ? $errors['passConfirm'] : '' }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="submit" name="regSubmit" class="btn btn-important" value="Registration">
                        </div>
                    </div>

                    <input type="hidden" name="autologin" value="1">

                </form>

            </div>
        </div>
    </div>
@endsection