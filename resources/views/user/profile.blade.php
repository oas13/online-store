@extends('layout.main')

@section('content')
    <div class="content">
        @if(session('userId') != null)
        <div class="panel-group checkout-accordions" id="profile-addresses">
            <div class="panel">
                <p id="title-p">Edit personal data <span style="color: greenyellow">{{$success}}</span></p>
                <div id="shipping-info" class="panel-collapse collapse in">
                    <div class="panel-body row">
                        <form action="/user/profile/edit" method="post">
                            @csrf
                            <div class="col-sm-4  form-group">
                                <label for="shipping_first_name">First Name</label>
                                <input type="text" name="name" id="shipping_first_name" value="{{ $user['name'] }}" class="form-control  "
                                       placeholder="first name*"/>
                                <span class="error"></span>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label for="shipping_last_name">Last Name</label>
                                <input type="text" name="surname" id="shipping_last_name" value="{{ $user['surname'] }}" class="form-control"
                                       placeholder="last name*"/>
                                <span class="error"></span>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label for="shipping_phone">Phone Number</label>
                                <input type="text" name="phone" id="shipping_phone" value="{{ $user['phone'] }}" class="form-control"
                                       placeholder="phone number"/>
                                <span class="error"></span>
                            </div>
                            <div class=" form-group col-xs-12 col-sm-4">
                                <label for="oldPassword">Old Password</label>
                                <input name="oldPassword" id="oldPassword" type="password" class=" form-control"
                                       value="" placeholder="Old Password"/>
                                <span class="error"></span>
                            </div>
                            <div class=" form-group col-xs-12 col-sm-4">
                                <label for="password">New Password</label>
                                <input name="newPassword" id="password" type="password" class=" form-control"
                                       value="" placeholder="New Password"/>
                                <span class="error"></span>
                            </div>
                            <div class=" form-group col-xs-12 col-sm-4">
                                <label for="passwordConfirm">Confirm Password</label>
                                <input name="passwordConfirm" id="passwordConfirm" type="password"
                                       class=" form-control" value="" placeholder="Confirm Password"/>
                                <span class="error"></span>
                            </div>
                            <div class=" form-group ">
                                <input type="submit" class="btn btn-important pull-right" value="Save Password" name="changeData">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        <!-- Using hasFeature helper method to check whether card variables should be accessed. -->

        {{--<div id="profile-edit-card">--}}
        {{--{{ partial('shop-customerprofile-edit-card') }}--}}
        {{--</div>--}}
        @else
        <div class="row">Please <a href="/user/login-page">login</a> to edit your profile.</div>
            @endif
        </div>
    </div>
@endsection