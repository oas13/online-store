@extends('layout.main')

@section('content')
<!-- INDEX PAGE CONTENT -->

    <div class="row content" id="intros">
        <!--BIG BG IMAGE-->
        <div id="intro">
            <div id="intro-left" class="col-md-6">
                <p></p>
            </div>
            <div id="intro-right" class="col-md-6">
                <p> </p>
            </div>
        </div>
    </div>

    <!--FEATURE BLOCK-->
    <div class="row content" id="feature">
        <p>Top products</p><br>
        @foreach($featuredProducts as $product)
        <div class="col-md-4 feature-item shop-item">
            <a href="/product/{{ $product->id }}">
                <div class="add_cart col-md-12">
                    <a href="/cart/add/product/{{ $product->id }}" class="btn btn-success">
                        Add to cart
                    </a>
                    <a href="/product/{{ $product->id }}" class="btn btn-info">Preview</a>
                </div>
                <img src="{{asset($product->img)}}" alt="">
                <div class="item-detail">
                    <h2>{{ $product->name }}</h2>
                    <small class="previous-price">
                        <i class="fa fa-dollar"></i>
                        {{ $product->old_price }}
                    </small>
                    <p>
                        <i class="fa fa-dollar"></i>
                        {{ $product->price }}
                    </p>
                </div>
            </a>
        </div>
        @endforeach
        {{--@foreach($featuredProduct as $product)--}}
            {{--@include('product.featured')--}}
        {{--@endforeach--}}
    </div>
    <!--END FEATURE BLOCK-->
    <div class="row content" id="intro-info">
        <div>
            <p>About</p><br>

            <div id="about-text" class="col-md-8">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ut mollis erat.
                    Nunc dignissim cursus orci quis fringilla. Quisque euismod odio ut convallis consequat.
                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ut mollis erat.
                    Nunc dignissim cursus orci quis fringilla. Quisque euismod odio ut convallis consequat.
                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                </p>
            </div>
            <div id="contact-text" class="col-md-4">
                <div>
                    <a href="http://instagram.com"><i class="fa fa-instagram"></i></a>
                    <a href="http://facebook.com/lemonstand"><i class="fa fa-facebook"></i></a>
                    <a href="http://twitter.com/D_Hanul_Park"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.pinterest.com"><i class="fa fa-pinterest"></i></a>
                    <br><br>
                </div>
                <div>
                    <p>support@slate.com</p>
                    <p>829-192-9382</p>
                    <p>9203 Pender St, Vancouver BC, J2K 2J2</p>
                </div>
            </div>
        </div>
    </div>
@endsection